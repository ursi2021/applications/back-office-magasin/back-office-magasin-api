var express = require('express');
var router = express.Router();
var app = express();
var logger = require('../logs');

var SSPController = require('./../controller/store_stock_position_controller');
var HelloController = require('./../controller/hello_controller');
var RestockingController = require('./../controller/restocking_controller');
var SellController = require('./../controller/sell_controller');
var ReferentielController = require('./../controller/referentiel_controller');
var SSMController = require('./../controller/store_stock_movement_controller');
var PromoController = require('./../controller/promotion_controller');

/**@swagger
 * /back-office-magasin/:
 *   get:
 *     summary: Back office magasin front page
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Return a view to the back office magasin
 *       404:
 *         description: Failed to view back office magasin
 */
router.get('/', HelloController.GetBackOffice);

/**@swagger
 * /back-office-magasin/hello:
 *   get:
 *     summary: Hello guys, how are u?
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Return a hello-world-!
 *       500:
 *         description: Failed to send a string
 */
router.get('/hello', HelloController.GetHello);

/**@swagger
 * /back-office-magasin/hello/all:
 *   get:
 *     summary: Hello guys, how are u?
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Return the Hello World of all application
 *       404:
 *         description: Failed to send/receive strings
 */
router.get('/hello/all', HelloController.GetAllHello);

/**@swagger
 * /back-office-magasin/clients:
 *   get:
 *     summary: Hello guys, how are u?
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Return a view to the clients list
 *       404:
 *         description: Failed to receive the clients
 */
router.get('/clients', HelloController.GetClients);

/**@swagger
 * /back-office-magasin/produits:
 *   get:
 *     summary: Hello guys, how are u?
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Return a view to the products list
 *       404:
 *         description: Failed to receive the products
 */
router.get('/produits', HelloController.GetProducts);

/**@swagger
 * /back-office-magasin/store_stock_position:
 *   get:
 *     tags:
 *       - Store
 *     summary: get stock position
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return store stock position
 *       500:
 *         description: Failed to retrieve data
 */
router.get('/store-stock-position', SSPController.FindAll);

/**@swagger
 * /back-office-magasin/store-restocking-request:
 *   get:
 *     summary: Hello guys, how are u?
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Return a the list of all restocks
 *       500:
 *         description: Failed to communicate with the db
 */
router.get('/store-restocking-request', RestockingController.GetAll);

/**@swagger
 * /back-office-magasin/restocking/init:
 *   get:
 *     summary: Hello guys, how are u?
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Return a the list of all restocks that were not sent yet
 *       500:
 *         description: Failed to communicate with the db
 */
router.get('/restocking/init', RestockingController.GetAllInit);

/**@swagger
 * /back-office-magasin/restocking/propose:
 *   get:
 *     summary: The GC are supposed to return a request about a specific restocking demand
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: return a string
 *       500:
 *         description: failed to return the const string
 */
router.post('/restocking/propose', RestockingController.Request);

/**@swagger
 * /back-office-magasin/referentielproduit:
 *   get:
 *     summary: redirect to referentielproduit
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: return the return of referentielproduit
 *       404:
 *         description: failed to communicate with referentielproduit
 */
router.get('/referentielproduit', HelloController.GetAxios);

/**@swagger
 * /back-office-magasin/store-sells:
 *   get:
 *     summary: send sells
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: return the sells of the day
 *       404:
 *         description: failed to communicate with the bdd
 */
router.get('/store-sells', SellController.Get);

/**@swagger
 * /back-office-magasin/products/store:
 *   get:
 *     summary: redirect to /products/store
 *     consumes:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: return the return of referentielproduit
 *       404:
 *         description: failed to communicate with referentielproduit
 */
router.get('/products/store', ReferentielController.GetProductStore)

/**@swagger
 * /back-office-magasin/store-stock-movement/insertStock:
 *   get:
 *     tags:
 *       - Store
 *     summary: insert new stock in store stock position, and update store stock movement
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return store stock movement
 *       500:
 *         description: Failed to put data (store stock Position)
 */
router.get('/store-stock-movement/insertStock', SSMController.InsertStock)

/**@swagger
 * /back-office-magasin/store-stock-movement/removeStock:
 *   get:
 *     tags:
 *       - Store
 *     summary: remove stock from store stock position, and update store stock movement
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return store stock movement
 *       500:
 *         description: Failed to put data (store stock movement)
 */
router.get('/store-stock-movement/removeStock', SSMController.RemoveStock)

/**@swagger
 * /back-office-magasin/store-stock-movement/removeStock:
 *   put:
 *     tags:
 *       - Store
 *     summary: remove stock from store stock position, and update store stock movement
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return store stock movement
 *       500:
 *         description: Failed to put data (store stock movement)
 */
router.put('/store-stock-movement/removeStock', SSMController.PutRemoveStock)

/**@swagger
 * /back-office-magasin/deliveries:
 *   get:
 *     tags:
 *       - Store
 *     summary: Get delivery list from Gestion Entrepot
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return deliveries
 *       500:
 *         description: Failed to communicate with Gestion Entrepot
 */
router.get('/deliveries', SSMController.GetDeliveries);

/**@swagger
 * /back-office-magasin/demandstock:
 *   get:
 *     tags:
 *       - Store
 *     summary: Get demand list from Gestion Entrepot
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return demands
 *       500:
 *         description: Failed to communicate
 */
router.get('/demandstock', SSPController.DemandStock);

/**@swagger
 * /back-office-magasin/SellReceive:
 *   post:
 *     tags:
 *       - Store
 *     summary: register a sell
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         ok
 *       500:
 *         fail on insert
 */
router.post('/SellReceive', SellController.Insert);

/**@swagger
 * /back-office-magasin/promo:
 *   post:
 *     tags:
 *       - Store
 *     summary: clock root for the promo
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         ok
 *       500:
 *         fail on insert
 */
router.post('/promo', PromoController.GetPromo);
module.exports = router;