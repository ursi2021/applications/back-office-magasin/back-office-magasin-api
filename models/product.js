const logger = require('../logs');
const { DataTypes } = require('sequelize');
var insert = require('../json/productModel.json');

const sequelize = require('./../service/connect_db');

const Product = sequelize.define('Product', {
  // Model attributes are defined here
  'product-code': {
    type: DataTypes.STRING,
    allowNull: false
  },
  'product-family': {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  'product-description': {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  'min-quantity': {
    type: DataTypes.INTEGER
    // allowNull defaults to true
  },
  'packaging': {
    type: DataTypes.INTEGER
    // allowNull defaults to true
  },
  'price': {
    type: DataTypes.FLOAT
    // allowNull defaults to true
  }
}, {
});


Product.sync().then(() => {
  logger.info("The table for the Product model was just (re)created!");
  Product.findAll().then(async prod => {
    if (prod.length == 0) {
      await Product.bulkCreate(insert);
      logger.info("Finished the json insertion");
    } else {
      logger.info("Skipped Insertion");
    }
  })
}).catch(e => {
  logger.log(e);
});


module.exports.Product = Product;