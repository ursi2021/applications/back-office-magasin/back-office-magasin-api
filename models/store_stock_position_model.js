const logger = require('../logs')
const { DataTypes } = require('sequelize');
var jsonFile = require('./../json/store_stock_position_model.json');
const sequelize = require('./../service/connect_db');

/**
 * @swagger
 * definitions:
 *  StoreStockPosition:
 *      type: object
 *      properties:
 *          store-id:
 *              type: int
 *          date:
 *              type: string
 *
 */
const StoreStockPosition = sequelize.define('StoreStockPosition', {
    'store-id': {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    date: {
        type: DataTypes.DATE,
        allowNull: false
    },

}, {
    timestamps: false
    // Other model options go here
});

/**
 * @swagger
 * definitions:
 *  SSPProductQuantity:
 *      type: object
 *      properties:
 *          product-code:
 *              type: string
 *          quantity:
 *              type: int
 *
 */
const SSPProductQuantity = sequelize.define('SSPProductQuantity', {
    'product-code': {
        type: DataTypes.STRING,
        allowNull: false
    },
    quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    'store-id' : {
        type : DataTypes.INTEGER,
        model: StoreStockPosition,
        key: 'id'
    }
}, {
    timestamps: false
});


StoreStockPosition.sync().then(() => {
    logger.info("The table for the StoreStockPosition model was just (re)created!");
    SSPProductQuantity.sync().then(() => {
        logger.info("The table for the SSPProductQuantity model was just (re)created!");
        StoreStockPosition.count().then(async count => {
            if (count === 0) {
                logger.info("Init the json insertion");
                var promise_tmp = [];
                var ssPosition = await StoreStockPosition.create(jsonFile);
                var productList = jsonFile['product-quantity-map'];
                for( let i = 0; i < productList.length; i++ ) {
                    var productQuantity = productList[i];
                    productQuantity['store-id'] = ssPosition['store-id'];
                    promise_tmp.push(SSPProductQuantity.create(productQuantity));
                }
                await Promise.all(promise_tmp);
                logger.info("Finished the json insertion");
            } else {
                logger.info("Skipped Insertion");
            }
        })
    })
}).catch(function (e) {
    console.log(e);
});

module.exports.StoreStockPosition = StoreStockPosition;
module.exports.SSPProductQuantity = SSPProductQuantity;
