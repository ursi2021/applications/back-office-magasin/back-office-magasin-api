const logger = require('../logs')
const { DataTypes } = require('sequelize');
const sequelize = require('./../service/connect_db');

/**
 * @swagger
 * definitions:
 *  StoreStockMovement:
 *      type: object
 *      properties:
 *          order_number:
 *              type: int
 *          entryDate:
 *              type: string
 *          receiptVoucher:
 *              type: string
 *          exitDate:
 *              type: string
 *          deliveryNote:
 *              type: string
 *
 */
const StoreStockMovement = sequelize.define('StoreStockMovement', {
    orderNumber: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    entryDate: {
        type: DataTypes.DATE,
        allowNull: false
    },
    receiptVoucher: {
        type: DataTypes.STRING,
        allowNull: false
    },
}, {
    timestamps: false
}
);

const SSMProductQuantity = sequelize.define('SSMProductQuantity', {
    'product-code': {
        type: DataTypes.STRING,
        allowNull: false
    },
    quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    storeStockMovementId : {
        type : DataTypes.INTEGER,
        model: StoreStockMovement,
        key: 'id'
    }
}, {
    timestamps: false
})

const SSMExitInvoice = sequelize.define('SSMExitInvoice', {
    exitDate: {
        type: DataTypes.DATE
    },
    invoice: {
        type: DataTypes.STRING
    },
    ssmProductId : {
        type : DataTypes.INTEGER,
        model: SSMProductQuantity,
        key: 'id'
    }
},{
    timestamps: false
})

StoreStockMovement.sync().then(() => {
    logger.info("The table for the StoreStockMovement model was just (re)created!");
    SSMProductQuantity.sync().then(() => {
        logger.info("The table for the SSMProductQuantity model was just (re)created!");
        SSMExitInvoice.sync().then(() => {
            logger.info("The table for the SSMExitInvoice model was just (re)created!");
        })
    })
});

module.exports.StoreStockMovement = StoreStockMovement;
module.exports.SSMProductQuantity = SSMProductQuantity;
module.exports.SSMExitInvoice = SSMExitInvoice;