const logger = require('../logs')
const { DataTypes } = require('sequelize');
var jsonFile = require('../json/restocking.json');

const sequelize = require('./../service/connect_db');

const STATES = {
    INIT : 0, //created
    SEND : 1, //sending response
    RECEIVE : 2, //receiving the answer
    PROPOSE : 3, //decide what to do
    DONE : 4 //Send the decision
};

const restocking = sequelize.define('restocking', {
    state: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue : STATES.INIT
    },
}, {
    // Other model options go here
});

const restockingProposition = sequelize.define('restockingProposition', {
    // Model attributes are defined here
    'product-code': {
        type: DataTypes.STRING,
        allowNull: false
    },
    'quantity': {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    'is-ge': {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
    'restocking-id' : {
        type : DataTypes.INTEGER,
        model: restocking,
        key: 'id'
    }
}, {
    // Other model options go here
});

restocking.sync().then(() => {
    logger.info("The table for the Restocking model was just (re)created!");
    restockingProposition.sync().then(() => {
        logger.info("The tables for the RestockingProposition model was just (re)created!");
        restocking.count().then(async count => {
            if (count == 0){
                logger.info("Init the json insertion");
                var promise_tmp = [];
                var data = jsonFile.data;
                for( let i = 0; i < data.length; i++ ) {
                    var restocking_json = data[i];
                    var acc_restocking = await restocking.create();
                    restocking_json.forEach(proposition => {
                        proposition["restocking-id"] = acc_restocking.id;
                        promise_tmp.push(restockingProposition.create(proposition));
                    });
                };
                await Promise.all(promise_tmp);
                logger.info("Finished the json insertion");
            } else {
                logger.info("Skipped Insertion");
            }
        }).catch(e => {
            console.log(e);
            logger.error("Error while inserting json to table");
        });
    });
}).catch(e => {
    logger.error(e);
});

module.exports.restocking = restocking;
module.exports.restockingProposition = restockingProposition;
module.exports.STATES = STATES;