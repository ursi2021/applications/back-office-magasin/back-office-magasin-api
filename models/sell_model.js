const { DataTypes } = require('sequelize');
var jsonFile = require('../json/sell_json.json');
const logger = require('../logs');

const sequelize = require('./../service/connect_db');

const sell = sequelize.define('sell', {
    product_code : {
        type: DataTypes.STRING,
        allowNull: false
    },
    quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},{
    // Other model options here
});

sell.sync().then(() => {
    logger.info("The table for the Sell model was just (re)created!");
    sell.count().then(async count => {
        if (count == 0){
            logger.info("Init the json insertion");
            var data = jsonFile.data;
            var promise_tmp = [];
            data.forEach(product => {
                promise_tmp.push(sell.create(product));
            });
            await Promise.all(promise_tmp);
            logger.info("Finished the json insertion");
        } else {
            logger.info("Skipped Insertion");
        }
    })
}).catch(e => {
    logger.error(e);
});

module.exports.sell = sell;
module.exports.sequelize = sequelize;