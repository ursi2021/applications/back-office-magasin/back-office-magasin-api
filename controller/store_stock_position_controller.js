var SSPService = require('../service/store_stock_position_service');

exports.FindAll = async function(req, res, next) {
    try {
        var storeStock = await SSPService.FindAll();
        res.status(200).json(storeStock);
    } catch (e) {
      res.status(500).json({'error': 'store stock position'});
    }
  };

exports.DemandStock = async function(req, res, next) {
  try {
    var demand = await SSPService.demandStock(req.query.storeId);
    res.status(200).json(demand);
  } catch (e) {
    console.log(e);
    res.status(500).json({'error' : 'store stock position'})
  }
}