let SellService = require('./../service/sell_service');
var SSMService = require('../service/store_stock_movement_service');
var SSPService = require('../service/store_stock_position_service');

exports.Get = async function(req, res){
    try {
        res.status(200).json(await SellService.Get());
    } catch (e) {
        res.status(404).json({'error': 'Sell'});
    }
}

exports.Insert = async function(req, res){
    try {
        var sells = req.body.sells;

        // Ajoute un exitDate au Store Stock Movement
        tmp_promise.push(SSMService.RemoveStock(sells));

        tmp_promise.push(SellService.Insert(sells));
        // Enlève la quantité achetée du Stock Store Position
        tmp_promise.push(SSPService.removeStock(sells, 1));
        var promise = await Promise.all(tmp_promise)
        res.status(200).json(promise[2]);
    } catch (e) {
        res.status(404).json({'error': 'Sell Insert'});
    }
}