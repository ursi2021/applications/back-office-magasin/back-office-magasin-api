let ReferentielService = require('./../service/referentiel_service');
const logger = require('../logs');

exports.GetProductStore = async function (req, res){
    try {
        res.status(200).json(await ReferentielService.GetProductsStore())
    } catch (e) {
        logger.error(e);
        res.status(400).json({"error" : "GetProductStore"})
    }
};