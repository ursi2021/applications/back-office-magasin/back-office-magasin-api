let PromotionService = require('./../service/promotion_service');

exports.GetPromo = async function(req, res, next) {
    try {
        let promos = await PromotionService.GetPromo();
        await PromotionService.UpdatePrice(promos);
        res.status(200).json({'ok':'ok'});
    } catch (e) {
        res.status(404).json({'error': 'GetPromo'});
    }
};