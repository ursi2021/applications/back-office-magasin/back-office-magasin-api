let RestockingService = require('./../service/restocking_service');

exports.GetAll =  async function(req, res, next){
        try {
          res.status(200).json(await RestockingService.GetAll());
        } catch (e){
          res.status(500).json({'error': 'Restocking', 'exc': e});
        }
      };

exports.GetAllInit = async function(req, res, next){
    try {
        var inits = await RestockingService.GetAllInit();
        res.status(200).json(inits);
    } catch (e){
        console.log(e);
        res.status(500).json({'error': 'Restocking'});
    }
};

exports.Request = async function(req, res, next){
    try {
        res.status(200).json('We still need to do this');
    } catch (e){
        console.log(e);
        res.status(500).json({'error': 'Restocking'});
    }
};