var SSMService = require('../service/store_stock_movement_service');
var SSPService = require('../service/store_stock_position_service');
var SellService = require('../service/sell_service');
let {GetData} = require('./../service/hello_service');

exports.InsertStock = async function(req, res, next) {
    try {
        //var newStockMovement = req.body;

        // Récupère les livraisons
        var url = process.env.URL_ENTREPROTS + '/delivery-shop/1';
        var newStockMovement = await GetData(url);
        for (let i = 0; i < newStockMovement.length; ++i) {
            var newStock = newStockMovement[i];
            await SSMService.InsertStock(newStock);
            var storeStock = await SSPService.addStock(newStock['product-quantity-map'], 1);
        }
        res.status(200).json(storeStock);
    } catch (e) {
        res.status(500).json({'error': 'update store stock movement'});
    }
}

exports.RemoveStock = async function(req, res, next) {
    try {
        // Récupère la vente de caisse
        var url = process.env.URL_CAISSE + '/store-sales';
        var tmp_promise = [];
        var sells = await GetData(url);

        // Ajoute un exitDate au Store Stock Movement
        tmp_promise.push(SSMService.RemoveStock(sells));

        tmp_promise.push(SellService.Insert(sells));
        // Enlève la quantité achetée du Stock Store Position
        tmp_promise.push(SSPService.removeStock(sells, 1));
        var promise = await Promise.all(tmp_promise)
        res.status(200).json(promise[2]);
    } catch (e) {
        res.status(500).json({'error': 'update store stock movement from caisse'});
    }
}

exports.PutRemoveStock = async function(req, res, next) {
    try {
        // Ajoute un exitDate au Store Stock Movement
        await SSMService.RemoveStock(req.body);

        // Enlève la quantité achetée du Stock Store Position
        var storeStock = await SSPService.removeStock(req.body, 1);
        res.status(200).json(storeStock);
    } catch (e) {
        res.status(500).json({'error': 'update store stock movement from caisse'});
    }
}

exports.GetDeliveries = async function(req, res, next) {
    try {
        var url = process.env.URL_ENTREPROTS + '/delivery-shop/1';
        res.status(200).json({deliveries : await GetData(url)});
    } catch (e) {
        res.status(404).json({'error': 'get deliveries from gestion entrepot'});
    }
};