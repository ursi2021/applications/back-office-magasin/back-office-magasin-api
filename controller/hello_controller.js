const { response } = require("express");
let HelloService = require('./../service/hello_service');
const logger = require("../logs");

exports.GetBackOffice = async function(req, res, next) {
    try {
        res.render('backOffice');
    } catch (e) {
        res.status(404).json({'error': 'backOffice'});
    }
};

exports.GetHello = async function(req, res, next){
    try {
      res.status(200).json({'back-office-magasin': "Hello World !"});
    } catch (e) {
      res.status(500).json({message: 'we are not available yet, plz contact us at: 060862xxxx'});
    }
  };

exports.GetAllHello = async function(req, res, next) {
    try {
        res.send({'back-office-magasin': await HelloService.getAll()})
    } catch (e){
        console.log(e);
        res.status(404).json({message: 'we are not available yet, plz contact us at: 060862xxxx'});
    }
  };

exports.GetClients = async function(req, res, next) {
    try {
        var url = process.env.URL_RELATION_CLIENT + '/clients';
        res.render('clients', { clients : await HelloService.GetData(url)});
    } catch (e) {
      res.status(404).json({'error': 'Clients'});
    }
  };

exports.GetProducts = async function(req, res, next) {
    try {
        var url = process.env.URL_REFERENTIEL_PRODUIT + '/produits' ;
        res.render('products', { products: await HelloService.GetData(url) });
    } catch (e) {
      res.status(404).json({'error': 'Produits'});
    }
  };

exports.GetAxios = async function(req, res, next){
  try {
    var url = process.env.URL_REFERENTIEL_PRODUIT;
    var ret = await HelloService.GetFromUrl(url + req.query.path, req.query.params);
    res.status(200).json(ret.data);
  } catch (e) {
    res.status(404).json({'error': 'Axios'});
  }
};