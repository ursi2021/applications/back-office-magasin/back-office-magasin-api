var express = require('express');
var router = express.Router();
var log = require('../logs');

router.get("", async (req, res, next) => { 
	try { 
		const jobs = [ // List of Treatments
			{ 	
				j: 0, // 0 is everyday
				h: 20, // 12h 
				m: 30, // 0 min
				date: null, // null because j = 0 (Recurrent Treatment) 
				name: "Daily sales", // Treatment's name 
				route: "/store-stock-movement/RemoveStock", // GET will be done on this route
				params : {
						//name : "test",
					}
			}, 
		]; 
		res.status(200).json(jobs);
	} catch (error) { 
		log.error(error); res.sendStatus(404); 
	} 
}); 

module.exports = router;
