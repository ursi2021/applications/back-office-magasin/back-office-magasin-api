let ReferentielService = require('./../service/referentiel_service');
let ProductModel = require('./../models/product');
let product = require('../json/productModel.json');
let axios = require('axios');

let dbProduct = [{
  "product-code": "4047443190659",
  "product-family": "Image et Son",
  "product-description": "X96 mini Smart TV Box 2+16 Go Lecteur Multimédia Android 7.1.2 Quad Core WIFI 4K H.265 HDR avec Indicateur LED",
  "min-quantity": 87,
  "packaging": 1,
  "price": 20,
  "assortment-type": "Both"
}]

jest.mock('axios');

jest.mock('../service/connect_db', () => {
    const Sequelize = require('sequelize');
    const sequelize = new Sequelize('database', 'username', 'password', {
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false
    });
    sequelize.authenticate();
    return sequelize;
  });
  
  beforeAll(async () => {
    await ProductModel.Product.sync();
    await ProductModel.Product.bulkCreate(dbProduct);
  })


test('Get Products Store', async () => {
    axios.get.mockResolvedValue({data : product});
    
    let res = await ReferentielService.GetProductsStore();
    expect(res.length).toBe(13);
    let prod = res.find(element => element['product-code'] === "4047443190659");
    expect(prod.price).toBe(28.17);
    //return 1;
  });
