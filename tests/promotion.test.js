let ProductModel = require('./../models/product');
let PromotionService = require('./../service/promotion_service');
let product = require('../json/productModel.json');
let axios = require('axios');
/*
  "product-code": "4047443190659",
  "price": 20,
*/
jest.mock('axios');

let merde = {"0":{"id":1,"start-date":"29-1-2021 6:35:27","end-date":"30-1-2021 6:35:27","offer-percentage":13,"offer-amount":null,"stores-list":["store1"],"promotion-type":"web","products-list":["4047443196927","4047443190659"],"minimum-amount":26,"description":"Random offer","clients-id":"646"},"1":{"id":2,"start-date":"29-1-2021 6:36:19","end-date":"30-1-2021 6:36:19","offer-percentage":9,"offer-amount":null,"stores-list":["store1"],"promotion-type":"leastsold","products-list":["4047443105876","4047443105875"],"minimum-amount":0,"description":"Offer for the two least sold products","clients-id":"495"},"2":{"id":3,"start-date":"29-1-2021 6:37:41","end-date":"30-1-2021 6:37:41","offer-percentage":100,"offer-amount":null,"stores-list":["store1"],"promotion-type":"pas web","products-list":["4047443190659"],"minimum-amount":20,"description":"Random offer","clients-id":null},"3":{"id":4,"start-date":"29-1-2021 6:37:41","end-date":"30-1-2021 6:37:41","offer-percentage":10,"offer-amount":null,"stores-list":["store1"],"promotion-type":"leastsold","products-list":["4047443105876","4047443105875"],"minimum-amount":0,"description":" ","clients-id":"237"}};
jest.mock('../service/connect_db', () => {
    const Sequelize = require('sequelize');
    const sequelize = new Sequelize('database', 'username', 'password', {
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false
    });
    sequelize.authenticate();
    return sequelize;
  });
  
  beforeAll(async () => {
    await ProductModel.Product.sync();
    await ProductModel.Product.bulkCreate(product);
  })

  test('Get Products Store', async () => {
    axios.get.mockResolvedValue({data : merde});

    let promos = await PromotionService.GetPromo();
    await PromotionService.UpdatePrice(promos);
    
    let res = await ProductModel.Product.findAll();
    let prod = res.find(element => element['product-code'] === "4047443190659");
    expect(prod.price).toBe(20);
  });