let ssp = require('./../service/store_stock_position_service');
let SMPModel = require('./../models/store_stock_position_model');
let json_ssp = require('../json/store_stock_position_model.json');

jest.mock('../service/connect_db', () => {
    const Sequelize = require('sequelize');
    const sequelize = new Sequelize('database', 'username', 'password', {
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false
    });
    sequelize.authenticate();
    return sequelize;
  });
  
  beforeAll(async () => {
    await SMPModel.SSPProductQuantity.sync();
    await SMPModel.StoreStockPosition.sync();

    var promise_tmp = [];
    var ssPosition = await SMPModel.StoreStockPosition.create(json_ssp);
    var productList = json_ssp['product-quantity-map'];
    for( let i = 0; i < productList.length; i++ ) {
        var productQuantity = productList[i];
        productQuantity['store-id'] = ssPosition['store-id'];
        promise_tmp.push(SMPModel.SSPProductQuantity.create(productQuantity));
    }
    await Promise.all(promise_tmp);
  })

  test('FIND', async () => {
    let res = await ssp.FindAll();
    expect(res.length).toBe(1);
    expect(res[0]['product-quantity-map'].length).toBe(13);
  });

  test('DEMAND', async () => {
    let res = await ssp.demandStock(1);
    expect(res.length).toBe(11);
  });
