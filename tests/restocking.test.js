let rs = require('./../service/restocking_service');
let RestockingModel = require('./../models/restocking');
let json_restocking = require('../json/restocking.json');


jest.mock('../service/connect_db', () => {
    const Sequelize = require('sequelize');
    const sequelize = new Sequelize('database', 'username', 'password', {
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false
    });
    sequelize.authenticate();
    return sequelize;
  });
  
  beforeAll(async () => {
    await RestockingModel.restocking.sync();
    await RestockingModel.restockingProposition.sync();
    
    var promise_tmp = [];
    var data = json_restocking.data;
    for( let i = 0; i < data.length; i++ ) {
        var restocking_json = data[i];
        var acc_restocking = await RestockingModel.restocking.create();
        restocking_json.forEach(proposition => {
            proposition["restocking-id"] = acc_restocking.id;
            promise_tmp.push(RestockingModel.restockingProposition.create(proposition));
        });
    };
    await Promise.all(promise_tmp);
  })

  test('Get All', async () => {
      let stock = await rs.GetAll();
      expect(stock['product-quantity-map'].length).toBe(1);
  });

  test('Get All Init', async () => {
    let stock = await rs.GetAllInit();
    expect(stock.length).toBe(2);
});