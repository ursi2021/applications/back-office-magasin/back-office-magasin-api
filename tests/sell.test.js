let ss = require('./../service/sell_service');
let SellModel = require('./../models/sell_model');
let json_sell = require('../json/sell_json.json');


jest.mock('../service/connect_db', () => {
    const Sequelize = require('sequelize');
    const sequelize = new Sequelize('database', 'username', 'password', {
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false
    });
    sequelize.authenticate();
    return sequelize;
  });
  
  beforeAll(async () => {
    await SellModel.sell.sync();
    await SellModel.sell.bulkCreate(json_sell.data);
  })

  test('Get All', async () => {
    let sales = await ss.Get();
    expect(sales['product-quantity-map'].length).toBe(5);
  });