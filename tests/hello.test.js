let HelloService = require('./../service/hello_service');
let axios = require('axios');

jest.mock('axios');

let gethello = {
  data :{
    test : 'hello'
  }
};
let getall = {
  data : {
    'relation-client' : 'hello',
    'decisionnel' : 'hello',
    'monetique-paiement' : 'hello',
    'gestion-promotion' : 'hello',
    'referentiel-produit' : 'hello',
    'gestion-commerciale' : 'hello',
    'e-commerce' : 'hello',
    'caisse' : 'hello',
    'gestion-entrepots' : 'hello',
  }
}

test('Get Hello', async () => {
    axios.get.mockResolvedValue(gethello);
    
    let hello = await HelloService.Get('', 'test');
    expect(hello).toBe('test : hello');
  });

test('Get All Hello', async () => {
    axios.get.mockResolvedValue(getall);
    
    let hellos = await HelloService.getAll();
    expect(hellos.length).toBe(9);
  });

test('Get Data', async () => {
    axios.get.mockResolvedValue(gethello);
    
    let res = await HelloService.GetData('test');
    expect(res.test).toBe('hello');
  });

test('Get From URL', async () => {
    axios.get.mockResolvedValue(gethello);
    
    let res = await HelloService.GetFromUrl('test', '');
    expect(res.data.test).toBe('hello');
  });