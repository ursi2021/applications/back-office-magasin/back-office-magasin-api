let ssm = require('./../service/store_stock_movement_service');
let SMMModel = require('./../models/store_stock_movement_model');


jest.mock('../service/connect_db', () => {
    const Sequelize = require('sequelize');
    const sequelize = new Sequelize('database', 'username', 'password', {
      dialect: 'sqlite',
      storage: ':memory:',
      logging: false
    });
    sequelize.authenticate();
    return sequelize;
  });
  
  beforeAll(async () => {
    await SMMModel.SSMExitInvoice.sync();
    await SMMModel.SSMProductQuantity.sync();
    await SMMModel.StoreStockMovement.sync();
  })

  test('Get All', async () => {
    expect(5).toBe(5);
  });