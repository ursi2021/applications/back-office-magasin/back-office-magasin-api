const logger = require('../logs')
var {StoreStockPosition, SSPProductQuantity} = require('../models/store_stock_position_model');
var {Product} = require('../models/product');
var RestockingService = require('./restocking_service');

exports.FindAll = async function(){
    var allStores = await StoreStockPosition.findAll();
    var ret = [];
    for (let i = 0; i < allStores.length; ++i) {
        var store = {};
        store['store-id'] = allStores[i]['store-id'];
        store.date = allStores[i].date;
        store['product-quantity-map'] = await SSPProductQuantity.findAll(
            {attributes: ['product-code', 'quantity'], where: {'store-id': allStores[i]['store-id']}});
        ret.push(store);
    }
    return ret;
};

exports.addStock = async function(newStock, storeId) {
    const foundStore = await StoreStockPosition.findOne({where: {'store-id': storeId}});
    if (!foundStore) {
        logger.error("no store found with id " + storeId);
        return await SSPProductQuantity.findAll();
    }
    await StoreStockPosition.update({date:Date.now()}, {where: {'store-id': storeId}});
    var promise_tmp = [];
    for (const newItem of newStock) {
        // First try to find the record
        var where = {where: { 'product-code': newItem['product-code'], 'store-id': storeId}};
        const foundItem = await SSPProductQuantity.findOne(where);
        if (!foundItem) {
            // Item not found, create a new one
            logger.info("create new Item in Store Stock Position");
            newItem['store-id'] = storeId;
            promise_tmp.push(SSPProductQuantity.create(newItem));
        } else {
            // Found an item, update it
            logger.info("update Item in Store Stock Position");
            newItem.quantity += foundItem.quantity;
            promise_tmp.push(SSPProductQuantity.update(newItem, where));
        }
    }
    await Promise.all(promise_tmp);
    return await SSPProductQuantity.findAll({where: {'store-id': storeId}});
};

exports.removeStock = async function(newStock, storeId) {
    const foundStore = await StoreStockPosition.findOne({where: {'store-id': storeId}});
    if (!foundStore) {
        logger.error("no store found with id " + storeId);
        return await SSPProductQuantity.findAll();
    }
    await StoreStockPosition.update({date:Date.now()}, {where: {'store-id': storeId}});
    var promise_tmp = [];
    for (let i = 0; i < newStock.length; ++i) {
        var removeStock = newStock[i]['product-quantity-map'];
        for (const removeItem of removeStock) {
            var where = {where: {'product-code': removeItem['product-code'], 'store-id': storeId}};
            const foundItem = await SSPProductQuantity.findOne(where);
            if (!foundItem) {
                logger.error("no Item found to remove in Store Stock Position");
                return await SSPProductQuantity.findAll({where: {'store-id': storeId}});
            }
            var quantity = foundItem.quantity - removeItem.quantity;
            if (quantity < 0) {
                logger.error("quantity to remove > than what is in Store Stock Position");
                return await SSPProductQuantity.findAll({where: {'store-id': storeId}});
            }
            removeItem.quantity = quantity;
            promise_tmp.push(SSPProductQuantity.update(removeItem, where));
            logger.info("Item found and removed from Store Stock Position");
        }
    }
    await Promise.all(promise_tmp);
    return await SSPProductQuantity.findAll({where: {'store-id': storeId}});
};

exports.demandStock = async function(storeId) {
    const foundStore = await StoreStockPosition.findOne({where: {'store-id': storeId}});
    if (!foundStore) {
        logger.error("no store found with id " + storeId);
        throw Error('No such store');
        //return await SSPProductQuantity.findAll();
    }
    //await StoreStockPosition.update({date:Date.now()}, {where: {'store-id': storeId}});
    var reassortProducts = await SSPProductQuantity.findAll({
        where : {
            'store-id': storeId
        }
    });
    var restocking_id = await RestockingService.CreateRestock();
    var promise = [];
    var res = [];
    for (var reassortItem of reassortProducts) {
        var foundItem = await Product.findOne({
            where : {
                'product-code': reassortItem["product-code"],
            }
        });
        if (foundItem["min-quantity"] >= reassortItem.quantity  ) {
            let quantity_to_add = foundItem["min-quantity"] * 2 - reassortItem.quantity;
            promise.push(RestockingService.InsertRestock(reassortItem["product-code"], quantity_to_add, restocking_id));
            res.push({"product-code" : reassortItem["product-code"], quantity : quantity_to_add});
        }
    }
    promise.push(RestockingService.UpdateRestock(restocking_id, 1));
    await Promise.all(promise);
    return res;
}