const logger = require('../logs')
var {StoreStockMovement, SSMProductQuantity, SSMExitInvoice} = require('../models/store_stock_movement_model');

exports.InsertStock = async function(newStockMovement){
    var newStock = {};
    newStock.entryDate = Date.now();
    newStock.receiptVoucher = "receipt"; //FIXME
    newStock.orderNumber = newStockMovement.id;
    newStock["product-quantity-map"] = newStockMovement["product-quantity-map"];
    const stockMovement = await StoreStockMovement.create(newStock);
    var promise_tmp = [];
    var productQuantityList = newStock["product-quantity-map"];

    productQuantityList.forEach(productQuantity => {
        productQuantity.storeStockMovementId = stockMovement.id;
        promise_tmp.push(SSMProductQuantity.create(productQuantity));
    })
    await Promise.all(promise_tmp);
    logger.info("Finished the store stock movement insertion");
    return stockMovement;
};

//dont ask just trust
exports.RemoveStock = async function(newStockMovement){
    var promise_tmp = [];
    for (let j = 0; j < newStockMovement.length; ++j) {
        for (const productQuantity of newStockMovement[j]['product-quantity-map']) {
            var quantityLeft = productQuantity.quantity;
            const ssmProductQuantityList = await SSMProductQuantity.findAll({where: {'product-code': productQuantity['product-code']}});
            if (!ssmProductQuantityList) {
                return await SSMExitInvoice.findAll();
            }
            while (quantityLeft > 0) {
                var foundItem = false;
                var i = 0;
                while (i < ssmProductQuantityList.length && !foundItem) {
                    const ssmProductQuantity = ssmProductQuantityList[i];
                    const countInvoices = await SSMExitInvoice.count({where: {'ssmProductId': ssmProductQuantity.id}});
                    if (ssmProductQuantity.quantity - countInvoices > 0) {
                        promise_tmp.push(SSMExitInvoice.create(
                            {
                                'exitDate': newStockMovement[i].date,
                                'invoice': "invoice",
                                'ssmProductId': ssmProductQuantity.id
                            }));
                        foundItem = true;
                    }
                    i++;
                }
                quantityLeft -= 1;
            }
        }
    }
    await Promise.all(promise_tmp);
    logger.info("Finished the store stock movement exitDate");
    return await SSMExitInvoice.findAll();
};