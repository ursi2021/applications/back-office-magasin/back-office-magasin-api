let sell = require('./../models/sell_model');
const { Op } = require('sequelize');

exports.Get = async function(){
    var date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);

    var list = await sell.sell.findAll({
        where : {
            createdAt : {
                [Op.gte] : date
            }
        }
    });
    
    res = {
        "product-quantity-map" : []
    };
    res_tmp = {
        "product-quantity-map" : {}
    };
    list.forEach(elem => {
        if(!res_tmp["product-quantity-map"][elem.product_code]){
            res_tmp["product-quantity-map"][elem.product_code] = {
                "product-code" : elem.product_code,
                quantity : 0
            }
        }
        res_tmp["product-quantity-map"][elem.product_code].quantity += elem.quantity
    });
    for (let key in res_tmp["product-quantity-map"]){
        res["product-quantity-map"].push(res_tmp["product-quantity-map"][key]);
    }
    res.date = new Date();
    return res;        
}

exports.Insert = async function(sells) {
    var promise = [];
    sells.forEach(sell_elem => {
        let elem = sell_elem["product-quantity-map"];
        for (let key in elem) {
            promise.push(sell.sell.create({product_code : elem[key]["product-code"], quantity : elem[key]["quantity"]}));
        }
    });
    await Promise.all(promise);
}