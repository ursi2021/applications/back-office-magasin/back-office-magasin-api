let {restocking, restockingProposition, STATES} = require('./../models/restocking');

exports.GetAll = async function(){
    var res = (await restocking.findAll({
        where : {
            state : STATES.INIT
        },
        order: [['createdAt', 'DESC']],
        limit: 1
    }))[0];
    var tmp = {};
    tmp["product-quantity-map"] = [];
    let st = await restockingProposition.findAll({
        where : {
            'restocking-id' : res.id,
            'is-ge' : false
        }
    });

    st.forEach(element => {
        let rest = { 'product-code': element['product-code'], 'quantity': element['quantity'] };
        tmp["product-quantity-map"].push(rest);
    })
    tmp.date = res.createdAt;
    return tmp;
};

exports.Accept = async function(){};
exports.Denie = async function(){};

exports.GetAllInit = async function(){
    var ret = [];
    var res = await restocking.findAll({
        where : {
            state : STATES.INIT
        }
    });
    for(let i = 0; i < res.length; i++){
        var acc = res[i];
        var tmp = {};
        tmp.our_proposal = await restockingProposition.findAll({
            where : {
                'restocking-id' : acc.id,
                'is-ge' : false
            }
        });
        tmp.state = acc.state;
        tmp.date = acc.createdAt;
        ret.push(tmp)
    }
    return ret;
};

exports.CreateRestock = async function(){
    let restock = await restocking.create();
    return restock.id;
}

exports.UpdateRestock = async function(id, state){
    await restocking.update({state: state}, {where : {id: id}});
}

exports.InsertRestock = async function(productCode, quantity, id){
    try {
        let tmp = await restockingProposition.create({'product-code' : productCode, quantity: quantity, "restocking-id" : id});
        return tmp;
    } catch (e) {
        return null;
    }
}