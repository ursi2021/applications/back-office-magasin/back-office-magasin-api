let axios = require('axios');

const list = [
    {url : process.env.URL_RELATION_CLIENT, key :'relation-client'},
    {url : process.env.URL_DECISIONNEL, key :'decisionnel'},
    {url : process.env.URL_MONETIQUE_PAIEMENT, key :'monetique-paiement'},
    {url : process.env.URL_GESTION_PROMOTION, key :'gestion-promotion'},
    {url : process.env.URL_REFERENTIEL_PRODUIT, key :'referentiel-produit'},
    {url : process.env.URL_GESTION_COMMERCIALE, key :'gestion-commerciale'},
    {url : process.env.URL_E_COMMERCE, key :'e-commerce'},
    {url : process.env.URL_CAISSE, key :'caisse'},
    {url : process.env.URL_ENTREPROTS, key :'gestion-entrepots'},
];

exports.Get = async function(url, key){
    try {
        var response = await axios.get(url);
        return (key + ' : ' + response.data[key]);
    } catch (e){
        console.log(e);
        return (key + ' : KO');
    }
};

exports.GetData = async function(url){
    try {
        var response = await axios.get(url);
        return response.data;
    } catch (e){
        console.log(e);
        return {};
    }
};

exports.GetFromUrl = async function(url, params){
    return await axios.get(url, {params : params});
}

exports.getAll = async function(){
    var result = [];

    list.forEach(elem => {
        result.push(this.Get(elem.url + '/hello', elem.key));
    });
    return await Promise.all(result);
};