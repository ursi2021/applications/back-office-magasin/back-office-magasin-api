let axios = require('axios');
let ProductModel = require('./../models/product');

exports.GetPromo = async function(){
    return (await axios.get(process.env.URL_GESTION_PROMOTION + '/offers')).data;
}

exports.UpdatePrice = async function(obj){
    let promos = [];

    for(let key in obj){
        promos.push(obj[key]);
    }
    
    for (let j = promos.length - 2 ; j < promos.length; j++) {
        let promo = promos[j];
        let min_amount = promo['minimum-amount'];
        let pourcentage = promo['offer-percentage'];
        let amount = promo['offer-amount'];
        let type = promo['promotion-type'];
        let client = promo['clients-id'];
        let products = promo['products-list'];

        if(type && type !== 'web' && !client){
            for (let i = 0; i < products.length; i++){
                let product = await ProductModel.Product.findOne({where : {
                    'product-code' : products[i]
                }});
                let price = pourcentage ? product.price * (1 - pourcentage / 100) : product.price - amount;
                price = price >= min_amount ? price : min_amount;
                await product.update({price});
            }
        }
    }
    return "I FELL GOOD, NANANANAAH";
}