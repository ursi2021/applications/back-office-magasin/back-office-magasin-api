// /referentiel-produit/products
let axios = require('axios');
let ProductModel = require('./../models/product');

exports.updateOrCreate = async function(model, where, newItem) {
    var elem = {
        'product-code' : newItem['product-code'],
        'product-family' : newItem['product-family'],
        'product-description' : newItem['product-description'],
        'min-quantity' : newItem['min-quantity'],
        packaging : newItem.packaging,
        price : newItem.price
    };
    // First try to find the record
   const foundItem = await model.findOne({where});
   if (!foundItem) {
        // Item not found, create a new one
        const item = await model.create(elem)
        return  {item, created: true};
    }
    // Found an item, update it
    const item = await model.update(elem, { where : {id : foundItem.id}});
    return {item, created: false};
}

exports.GetProductsStore = async function(){
    var url = process.env.URL_REFERENTIEL_PRODUIT;
    var res = (await axios.get(url + "/products/store")).data;
    var promise = [];
    res.forEach(elem => {
        var tmp = elem["product-code"] || elem["product_code"];
        promise.push(exports.updateOrCreate(ProductModel.Product, {'product-code' : tmp}, elem))
    });
    await Promise.all(promise);
    return await ProductModel.Product.findAll();
};